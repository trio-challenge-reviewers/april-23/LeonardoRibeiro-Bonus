package com.trio.TrioAuctionProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TrioAuctionProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(TrioAuctionProjectApplication.class, args);
	}

}
