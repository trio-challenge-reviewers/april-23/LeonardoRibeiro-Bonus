package com.trio.TrioAuctionProject.controller;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.trio.auction.model.Bidder;

import java.util.List;

public class AuctionRequest {
    @JsonProperty("item")
    private String item;

    @JsonProperty("bidders")
    private List<Bidder> bidders;

    public String getItem() {
        return item;
    }

    public void setItem(String item) {
        this.item = item;
    }

    public List<Bidder> getBidders() {
        return bidders;
    }

    public void setBidders(List<Bidder> bidders) {
        this.bidders = bidders;
    }
}
