package com.trio.TrioAuctionProject.controller;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.trio.auction.model.AuctionItem;
import com.trio.auction.model.AuctionResult;
import com.trio.auction.service.AuctionService;

@RestController
@RequestMapping("/auction")
public class AuctionController {

	@Autowired
	private AuctionService auctionService;
	
	@GetMapping("/health")
    @ResponseBody
    public String healthCheck() {
        return "OK";
    }
	
	
	@PostMapping("/processAuction")
	public ResponseEntity<List<AuctionResult>> processAuction(@RequestBody List<AuctionRequest> auctionRequests) {

	    List<AuctionItem> auctionItems = auctionRequests.stream()
	            .map(request -> new AuctionItem(request.getItem(), request.getBidders())).collect(Collectors.toList());

	    List<AuctionResult> results = auctionItems.stream()
	            .map(auctionService::runAuction)
	            .filter(Optional::isPresent)
	            .map(Optional::get)
	            .collect(Collectors.toList());

	    if (!results.isEmpty()) {
	        return ResponseEntity.ok(results);
	    } else {
	        return ResponseEntity.notFound().build();
	    }
	}

}