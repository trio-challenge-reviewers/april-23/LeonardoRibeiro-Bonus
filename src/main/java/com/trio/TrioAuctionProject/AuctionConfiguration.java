package com.trio.TrioAuctionProject;

import org.springframework.context.annotation.Configuration;

import com.trio.auction.processor.AuctionProcessorFactory;
import com.trio.auction.processor.IAuctionProcessor;
import com.trio.auction.service.AuctionService;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;

@Configuration
public class AuctionConfiguration {

	@Value("${auction.log.filename}")
    private String auctionLogFilename;
	
	@Bean
    public IAuctionProcessor auctionProcessor() {
        return AuctionProcessorFactory.createDefaultProcessor(auctionLogFilename);
    }

    @Bean
    public AuctionService auctionService(IAuctionProcessor auctionProcessor) {
        return new AuctionService(auctionProcessor);
    }
}
